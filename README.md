# Synchronizer

This repository contains the implementation of a synchronizer for synchronizing `x` programs. It is a static `C` and `C++` library.
The interface provides the following functions:

* init_sync(): initialize the synchronizer
* do_sync(): do synchronization, e.g. "wait" for other programs to reach this point
* cleanup_sync(): cleanup the synchronizer (e.g. close shared memory, semaphores)
* force_cleanup(): closes and deallocates all resources (useful if there was a program failure and the synchronizer stays in a wrong state)

The `do_sync()` method can be called as many times as there should be a synchronization between two programs.

## Example


An example program with two synchronization points (see `dir/program.cpp`)
```
#include "sync.h"

int main(int argc, char *argv[]) {
    
    // initialize synchronizer
    init_sync();

    // preprocessing
    printf("Starting program\n");

    // synchronization point
    do_sync();

    // do work
    sleep(4);

    // synchronization point
    do_sync();

    // postprocessing
    printf("Done\n");

    // cleanup peacefully
    cleanup_sync();

}
```

## Prerequisites

* Environment Variable: setting the number of programs (`x`) containing synchronized calls as the environment variable's `NUM_SYNC_PROGRAMS` value:
`export NUM_SYNC_PROGRAMS="x"`
* Makefile (see `dir/Makefile`)
    * Linking the directory with the code of the library implementation with `-L` and `-I`
    * Adding `-lsynclib -lpthread -lrt` for the compilation

/**
 * @file    main.c
 * @brief   The implementation of sync.h.
 *
 * Implements the function declarations from sync.h.
 * Represents the source code for the synchronizer.
 * The environment variable NUM_SYNC_PROGRAMS needs to be set to the number of programs, that should be synchronized.
 */

#include "sync.h"

static sem_t *s_private;
static sem_t *sem_all;
static sem_t *s_init;
static struct sync_shm *shared;
static int programs;
static int shmfd;

int init_sync() {
	programs = atoi(getenv("NUM_SYNC_PROGRAMS"));
	int shm_new = 1;
	
	// only allow s_init currently
	s_private = sem_open(SEM_PRIVATE, O_CREAT, PERMISSION, 0);
	sem_all = sem_open(SEM_ALL, O_CREAT, PERMISSION, 0);
	s_init = sem_open(SEM_INIT, O_CREAT, PERMISSION, 1);

	// make sure, that the one thread creating the shared memory also initializes shared->var before all other threads
	sem_wait(s_init);

	shmfd = shm_open(SHM_NAME, O_RDWR | O_CREAT | O_EXCL, PERMISSION);
	if (shmfd == -1) {
		// either already created or real error
		shmfd = shm_open(SHM_NAME, O_RDWR | O_CREAT, PERMISSION);
		if (shmfd == -1) {
			// real error
			fprintf(stderr, "Error at opening shared memory\n");
			return -1;
		}
		// otherwise already created
		shm_new = 0;
	}
	
	if (ftruncate(shmfd, sizeof(*shared)) == -1) {
		fprintf(stderr, "Error ftruncate\n");
		return -1;
	}

	shared = mmap(NULL, sizeof(*shared), PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);
	if (shared == MAP_FAILED) {
		fprintf(stderr, "mmap failed\n");
		return -1;
	}

	if (shm_new == 1) {
		shared->var = 0;
		for (int i = 0; i < programs - 1; i++) {
			sem_post(s_init);
		}
		// do_sync() method possible after initializing shared->var
		sem_post(s_private);
	}

	// this post is theoretically not needed since we want to sync <programs> applications
	// after the 'master' thread, there will be <programs-1> s_init waits possible / available
	sem_post(s_init);

	return 0;
}

int do_sync() {

	sem_wait(s_private);

	shared->var = shared->var + 1;
	if (cleanUp == 1) {
		// only the program which was responsible for the cleanUp of the previous sync will fulfill this condition
		// since it will be set later again to that thread which will be responsible later, we can set cleanUp to the default value 0 again
		cleanUp = 0;
	}

	if (shared->var == programs) {
		for (int i = 0; i < shared->var; i++) {
			sem_post(sem_all);
		}
		cleanUp = 1;
		// set to zero in case there will be another sync
		shared->var = 0;
	}

	sem_post(s_private);

	// now wait directly
	sem_wait(sem_all);

	return 0;
}

int cleanup_sync() {
	if (munmap(shared, sizeof *shared) == -1) {
		fprintf(stderr, "munmap error\n");
		return -1;
	}

	if (close(shmfd) == -1) {
		fprintf(stderr, "close failed\n");
		return -1;
	}

	if (cleanUp) {
		if (shmfd != -1) {
			if (shm_unlink(SHM_NAME) == -1) {
				fprintf(stderr, "unlink failed\n");
			}
		}
	}
	
	sem_close(s_private);
	sem_close(sem_all);
	sem_close(s_init);

	if (cleanUp) {
		sem_unlink(SEM_ALL);
		sem_unlink(SEM_PRIVATE);
		sem_unlink(SEM_INIT);
	}

	return 0;
}

int force_cleanup() {

	munmap(shared, sizeof *shared);
	close(shmfd);
	shm_unlink(SHM_NAME);
	
	sem_close(s_private);
	sem_close(sem_all);
	sem_close(s_init);

	sem_unlink(SEM_ALL);
	sem_unlink(SEM_PRIVATE);
	sem_unlink(SEM_INIT);
	
	return 0;
}

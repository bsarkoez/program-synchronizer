/**
 * @file    sync.h
 * @brief   Header file of the synchronizer.
 *
 * This file contains variable and method declarations for the synchronizer.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <semaphore.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>

#define SEM_ALL "/semAll"
#define SEM_PRIVATE "/semPreprocessing"
#define SEM_INIT "/semInit"
#define SHM_NAME "/shmSync"
#define PERMISSION (0600)

/**
 * Variable used for the thorough clean up of resources.
 * (not only closing, but de-allocating the memory)
 */
static volatile sig_atomic_t cleanUp = 0;

/**
 * The shared memory struct used for the synchronization.
 * The variable var is used for the number of programs already synchronized.
 */
struct sync_shm {
	int var;
};


#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief   Initializes the sync procedure.
 *
 * All semaphores, the shared memory variable, etc. is initialized and set up.
 * @return  0 in case of a success, -1 otherwise.
 */
int init_sync();

/**
 * @brief   Does the synchronization with other programs.
 *
 * Waits on its semaphore sem_all such that all other programs achieve this synchronization point.
 * If the last program arrives, the semaphore sem_all will be posted as many times as there are programs to synchronize.
 * @return  0 in case of a success, -1 otherwise.
 */
int do_sync();

/**
 * @brief   Cleans up the used resources.
 *
 * All resources will be closed gracefully. The thread/program with the cleanUp variable additionally de-allocates
 * the used resources.
 * @return  0 in case of a success, -1 otherwise.
 */
int cleanup_sync();

/**
 * @brief   Forces a clean up.
 *
 * In case we need a forced clean up with not only closing, but additionally de-allocating all resources, we can use
 * this function.
 * @return  0 in case of a success.
 */
int force_cleanup();

#ifdef __cplusplus
}
#endif

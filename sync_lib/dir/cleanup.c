/**
 * Cleanup program which invokes a forced cleanup.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "sync.h"

int main(int argc, char *argv[]) {

	init_sync();
	
	force_cleanup();


}

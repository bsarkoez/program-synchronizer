/**
 * Example C code for using the synchronizer once.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "sync.h"

int main(int argc, char *argv[]) {

	srand(getpid()); 
	int r = rand() % 4;

	init_sync();

	// do something
	printf("start sleeping %d\n", r);
		
	sleep(r);
	
	do_sync();
	printf("juhuuu work continues\n");
	cleanup_sync();

}

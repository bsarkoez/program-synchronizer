/**
 * Example C++ code for using the synchronizer twice.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "sync.h"

int main(int argc, char *argv[]) {

	srand(getpid()); 
	int r = rand() % 4;

	init_sync();

	// do something
	printf("start sleeping in c++ %d\n", r);
		
	sleep(r);
	
	do_sync();
	
	r = rand() % 5;
	printf("sleep as work before second sync %d\n", r);
	sleep(r);
	printf("sleep done\n");

	do_sync();

	printf("juhuuu work continues\n");
	cleanup_sync();

}
